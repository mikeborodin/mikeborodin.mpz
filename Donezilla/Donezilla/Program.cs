﻿using System;

namespace adonetcore
{
    class Program
    {


        static void Main(string[] args)
        {

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("################\n"+
                              "### WELCOME! ###\n"+
                              "################\n" );

            var db = new DbHelper();
            while(true){
                var command = Console.ReadLine();
                
                switch(command){
                    case "ls":
                    db.list();
                    break;

                    case "add":
                    db.add();
                    break;
                    case "rm":
                    db.rm();
                    break;

                    case "upd":
                    db.upd();
                    break;

                    case "find":
                    db.find();
                    break;


                    default:
                    break;
                }
            }
        }
    }
}
