﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Donezilla.db;
using Donezilla.db.statuses;

namespace Donezilla
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private DbHelper db = new DbHelper();

        private void ShowDetails(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Test");
            Position p = ((FrameworkElement)sender).DataContext as Position;
            var empWindow = new EmployeesAtPositionWindow(p.Id);
            empWindow.Show();
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            ObservableCollection<Position> Positions = (DataContext as DataModel).Tasks;
            Positions.Clear();
            db.GetPositionList().ForEach((Position p) =>
            {
                Positions.Add(p);
            });
        }

        private void AddPosition(object sender, RoutedEventArgs e)
        {
            db.addPosition(new Position(){Title = "A", Description = "The Architect of everything"});
            MessageBox.Show("Position Added");
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            Position p = ((FrameworkElement)sender).DataContext as Position;
            db.deletePosition(p);
        }

        private void UpdatePosition(object sender, RoutedEventArgs e)
        {
            Position p = ((FrameworkElement)sender).DataContext as Position;
            db.updatePosition(p);
        }
    }
}
