using System;
using System.Data.SqlClient;
using System.Text;

namespace adonetcore
{
    public class DbHelper{
static string CONN = "Data Source=127.0.0.1\\2a1642fd64e5,1500;Initial Catalog=lab;User ID=sa;Password=zxcvbnm#$%123";

        public void list(){
            try 
            { 
                //jdbc:jtds:sqlserver://itcompanyserver.database.windows.net:1433/itcompany;user=mikeborodin;password=zxcvbnmSER123;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "127.0.0.1,1500";
                builder.UserID = "sa";            
                builder.Password = "zxcvbnm#$%123";     
                builder.InitialCatalog = "lab"; 
                
                builder.ConnectionString = "";

                
                using (SqlConnection connection = new SqlConnection(CONN))
                { 
                    connection.Open();       
                    Console.WriteLine("Connection established!");

                    using (SqlCommand command = new SqlCommand("SELECT * FROM positions", connection))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine("{0}\t{1}\t{2}", reader.GetInt64(0), reader.GetString(1),reader.GetString(2));
                            }
                        }
                    }
                    connection.Close();                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            } 
        }


 public void find(){
            try 
            { 
                //jdbc:jtds:sqlserver://itcompanyserver.database.windows.net:1433/itcompany;user=mikeborodin;password=zxcvbnmSER123;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "127.0.0.1,1500";
                builder.UserID = "sa";            
                builder.Password = "zxcvbnm#$%123";     
                builder.InitialCatalog = "lab"; 
                
                builder.ConnectionString = "";

                
                using (SqlConnection connection = new SqlConnection(CONN))
                { 
                    connection.Open();       
                    Console.WriteLine("Input search, please:");

                    var s = Console.ReadLine();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM positions WHERE title LIKE '%"+s+"%' OR description LIKE '%"+s+"%'", connection))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine("{0}\t{1}\t{2}", reader.GetInt64(0), reader.GetString(1),reader.GetString(2));
                            }
                        }
                    }

                    Console.WriteLine("Done");
                    connection.Close();                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            } 
        }



        public void add(){
            try 
            { 
                //jdbc:jtds:sqlserver://itcompanyserver.database.windows.net:1433/itcompany;user=mikeborodin;password=zxcvbnmSER123;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "127.0.0.1,1500";
                builder.UserID = "sa";            
                builder.Password = "zxcvbnm#$%123";     
                builder.InitialCatalog = "lab"; 

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Input position title");
                var pTitle = Console.ReadLine();          
                Console.WriteLine("Input position description");                      
                var pDesc = Console.ReadLine();                
                
                using (SqlConnection connection = new SqlConnection(CONN))
                { 
                    connection.Open();       
                    Console.WriteLine("OK");

                    using (SqlCommand command = new SqlCommand("INSERT INTO  positions (title,description) VALUES ('"+pTitle+"','"+pDesc+"')", connection))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        command.ExecuteNonQuery();
                        // using (SqlDataReader reader = command.ExecuteReader())
                        // {
                        //     while (reader.Read())
                        //     {
                        //         Console.WriteLine("{0}\t{1}\t{2}", reader.GetInt64(0), reader.GetString(1),reader.GetString(2));
                        //     }
                        // }
                    }
                    Console.WriteLine("Done");
                    connection.Close();                    
                }
            }
            catch (SqlException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.ToString());
            } 
        }


         public void rm(){
            try 
            { 
                //jdbc:jtds:sqlserver://itcompanyserver.database.windows.net:1433/itcompany;user=mikeborodin;password=zxcvbnmSER123;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "127.0.0.1,1500";
                builder.UserID = "sa";            
                builder.Password = "zxcvbnm#$%123";     
                builder.InitialCatalog = "lab"; 

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Input position ID to delete");
                var pId = Console.ReadLine();             
                
                using (SqlConnection connection = new SqlConnection(CONN))
                { 
                    connection.Open();       
                    //Console.WriteLine("OK");

                    using (SqlCommand command = new SqlCommand("DELETE FROM positions WHERE id="+pId, connection))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        command.ExecuteNonQuery();
                        // using (SqlDataReader reader = command.ExecuteReader())
                        // {
                        //     while (reader.Read())
                        //     {
                        //         Console.WriteLine("{0}\t{1}\t{2}", reader.GetInt64(0), reader.GetString(1),reader.GetString(2));
                        //     }
                        // }
                    }
                    Console.WriteLine("Done");
                    connection.Close();                    
                }
            }
            catch (SqlException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.ToString());
            } 
        }




          public void upd(){
            try 
            { 
                //jdbc:jtds:sqlserver://itcompanyserver.database.windows.net:1433/itcompany;user=mikeborodin;password=zxcvbnmSER123;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "127.0.0.1,1500";
                builder.UserID = "sa";            
                builder.Password = "zxcvbnm#$%123";     
                builder.InitialCatalog = "lab"; 

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Input position ID to update");
                var pId = Console.ReadLine();             
                
                Console.WriteLine("Input new position title");
                var pTitle = Console.ReadLine();          
                Console.WriteLine("Input new position description");                      
                var pDesc = Console.ReadLine(); 
                
                using (SqlConnection connection = new SqlConnection(CONN))
                { 
                    connection.Open();       
                    //Console.WriteLine("OK");

                    using (SqlCommand command = new SqlCommand("UPDATE positions SET title='"+pTitle+"',description='"+pDesc+"' WHERE id="+pId, connection))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        command.ExecuteNonQuery();
                        // using (SqlDataReader reader = command.ExecuteReader())
                        // {
                        //     while (reader.Read())
                        //     {
                        //         Console.WriteLine("{0}\t{1}\t{2}", reader.GetInt64(0), reader.GetString(1),reader.GetString(2));
                        //     }
                        // }
                    }
                    Console.WriteLine("Done");
                    connection.Close();                    
                }
            }
            catch (SqlException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.ToString());
            } 
        }



    }

}