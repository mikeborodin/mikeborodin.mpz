﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Donezilla.db;
using Donezilla.db.dlinq;
using Donezilla.db.entity;

namespace Donezilla
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private DataModel Model;
        private DbHelper dbAdoHelper = new DbHelper();
        public App()
        {

            /*Model = new DataModel();
            Model.Tasks = new ObservableCollection<Position>();
            dbAdoHelper.GetPositionList().ForEach((pos=>
                Model.Tasks.Add(pos)
            ));*/

            /*
             var w = new MainWindow(){DataContext = Model};
             w.Show();
             
            var w = new LinqWindow();
            w.Show();*/


            var efWin = new EFWindow();
            efWin.Show();
        }

    }
}
