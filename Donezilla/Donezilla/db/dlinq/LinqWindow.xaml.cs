﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Donezilla.db.dlinq
{
    /// <summary>
    /// Interaction logic for LinqWindow.xaml
    /// </summary>
    public partial class LinqWindow : Window
    {
        public ObservableCollection<employee> Employees { get; set; }
        itcompanyDataContext db = new itcompanyDataContext(DbHelper.CONN);

        public LinqWindow()
        {
            InitializeComponent();
            DataContext = db;
        }


        
        public void AddEmployee()
        {
            db.employees.InsertOnSubmit(new employee()
            {
                name = "Bob",
                lastname = "Marley",
                positionId = 0,
            });
            db.SubmitChanges();
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            
        }

        private void LevelUp(object sender, RoutedEventArgs e)
        {

            employee p = ((FrameworkElement)sender).DataContext as employee;
            MessageBox.Show("Delete emeployee "+p.lastname);
            db.employees.DeleteOnSubmit(p);
            //db.SubmitChanges();
        }
    }
}
