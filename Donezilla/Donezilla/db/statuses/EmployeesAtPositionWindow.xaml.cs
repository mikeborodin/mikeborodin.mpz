﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Donezilla.db.statuses
{
    /// <summary>
    /// Interaction logic for StatusWorkers.xaml
    /// </summary>
    public partial class EmployeesAtPositionWindow : Window
    {
        public ObservableCollection<Employee> Employees { get; set; } = new ObservableCollection<Employee>();
        private DbHelper db  = new DbHelper();
        public EmployeesAtPositionWindow(long positionId)
        {
            InitializeComponent();
            DataContext = this;
            db.GetPositionWorkersList(positionId).ForEach((e=> 
            Employees.Add(e)
            ));
        }
    }
}
