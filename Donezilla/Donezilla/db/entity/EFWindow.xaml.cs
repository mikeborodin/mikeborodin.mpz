﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Donezilla.db.entity
{
    /// <summary>
    /// Interaction logic for EFWindow.xaml
    /// </summary>
    public partial class EFWindow : Window
    {

        public EFWindow()
        {
            InitializeComponent();
        }

        private void ShowDetails(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            labEntities db = new db.entity.labEntities();
            position position = new position();
            position.id = 1234;
            position.description = "New job opens";
            position.title = "Data Schientist";
            db.positions.Remove(position);
            db.SaveChanges();
        }

        private void UpdatePosition(object sender, RoutedEventArgs e)
        {

            labEntities db = new db.entity.labEntities();
            position position = new position();
            position.id = 1234;
            position.description = "New job opens";
            position.title = "Data Schientist";
            db.positions.Add(position);
            db.SaveChanges();
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            labEntities db = new db.entity.labEntities();
           
            var q = from it in db.positions select it;


            foreach (position p in q)
            {
              Console.WriteLine(p.title);  
            } 

        }

        private void AddPosition(object sender, RoutedEventArgs e)
        {


            labEntities db = new db.entity.labEntities();
            position position = new position();
            position.id = 1234;
            position.description = "New job opens";
            position.title = "Data Schientist";
            db.positions.Add(position);
            db.SaveChanges();


        }
    }
}
