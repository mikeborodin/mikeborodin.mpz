﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using Donezilla.db;
using Service;

namespace Client
{
	public class MyServiceClient : ClientBase<IITCompanyService>, IITCompanyService
	{
		public MyServiceClient (Binding binding, EndpointAddress address) : base (binding, address)
		{
		}

		public string Greet (string name)
		{
			return Channel.Greet (name);
		}

		public bool SignIn (string email, string pass)
		{
			return Channel.SignIn(email,pass);
		}

		public  bool SignOut (){
			return Channel.SignOut ();
		}


		public void CreatePosition(string title,string description){
			Channel.CreatePosition( title,  description);
		}

	    public async Task<List<Position>> GetPositions()
	    {
	        return await Channel.GetPositions();
	    }

	    public async void UpdatePosition(Position position)
	    {
	        Channel.UpdatePosition(position);
	    }

	    public async void DeletePosition(Position position)
	    {
	        Channel.DeletePosition(position);
	    }




	    public bool CheckPositionIsValid(Position position)
	    {
	        throw new NotImplementedException();
	    }

	    public void SetVacantPosition(Position position)
	    {
	        throw new NotImplementedException();
	    }

	    public void SendEmailVerification(string email)
	    {
	        throw new NotImplementedException();
	    }
	}
}