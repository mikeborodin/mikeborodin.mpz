﻿// Client
using System;
using System.Collections.Generic;
using System.ServiceModel;
using Donezilla.db;
using Service;

namespace Client
{
	public class Program
	{
		public static void Main (string[] args)
		{


			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine ("Welcome to Donezilla WCF Client!\n");
			string title = "";
			string description = "";
			Console.ForegroundColor = ConsoleColor.Blue;


			var binding = new BasicHttpBinding ();
			var address = new EndpointAddress ("http://localhost:8080");
			var client = new MyServiceClient (binding, address);


			if (client.SignIn ("test@test.com","1111")) {
				Console.Write("\nLogged in successfully");
			}

			while (true)
			{
            
			    string op = Console.ReadLine();
			    switch (op)
			    {
                    case "hi":
                        Console.Write("Hello! I'm Donezilla! How can I help?\n (list, add, alter, delete)\n");
                        break;

                    case "add":

                        Console.Write("\nEnter New Position Title: ");
                        title = Console.ReadLine();
                        Console.Write("\nEnter New Position Description: ");
                        description = Console.ReadLine();
                        client.CreatePosition(title, description);

                        break;

                    case "list":
                        client.GetPositions().ContinueWith(l =>
                        {
                            l.Result.ForEach(p =>
                            {
                                //Console.BackgroundColor = ConsoleColor.Cyan;
                                Console.ForegroundColor = ConsoleColor.Cyan;
                                Console.WriteLine("\nId: " + p.Id+ "\title: " + p.Title + "\tDescription\t" + p.Description);
                            });
                        });
                        break;

			        case "alter":

			            Console.WriteLine("What to alter? (int)\n");

                        int uId = 0;
                        int.TryParse(Console.ReadLine(), out uId);



                        Console.WriteLine("New title\n");
			            string uTitle = Console.ReadLine();
			            Console.WriteLine("New title\n");
                        string uDescription = Console.ReadLine();

			            Console.WriteLine("New List:\n");
                        client.GetPositions().ContinueWith(l =>
			            {
			                l.Result.ForEach(p =>
			                {
			                    //Console.BackgroundColor = ConsoleColor.Cyan;
			                    Console.ForegroundColor = ConsoleColor.Cyan;
			                    Console.WriteLine("\nId: " + p.Id + "\title: " + p.Title + "\tDescription\t" + p.Description);
			                });
			            });


                        break;

                        
			        case "rm":

			            Console.WriteLine("What to rempove? (int)\n");

			            int dId = 0;
			            int.TryParse(Console.ReadLine(), out dId);
                        client.DeletePosition(new Position()
                        {
                            Id = dId
                        });

                        break;

                    default:
                        Console.WriteLine("Try `hi` command!\n");
                    break;
			    }
				
			}



		}

	    public class OnReadyIml : OnReadyCallback
	    {
	        public override void OnReady(object o)
	        {
	            List<Position> positions = (List<Position>)o;
                positions.ForEach(p=>
                {
                    //Console.BackgroundColor = ConsoleColor.Cyan;
                    Console.ForegroundColor= ConsoleColor.Cyan;
                    Console.WriteLine("Title: "+p.Title+"\tDescription\t"+p.Description);
                });

	        }
	    }
    }
}

