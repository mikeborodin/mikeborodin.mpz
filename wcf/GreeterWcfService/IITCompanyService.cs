﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Donezilla.db;

namespace Service
{
	[ServiceContract]
	public interface IITCompanyService
	{
		[OperationContract]
		string Greet (string name);

		[OperationContract]
		bool SignIn (string email,string pass);

		[OperationContract]
		bool SignOut ();


		[OperationContract]
		void CreatePosition(string title,string description);


	    [OperationContract]
        Task<List<Position>> GetPositions();


	    [OperationContract]
	    void UpdatePosition(Position position);

	    [OperationContract]
	    void DeletePosition(Position position);


	    [OperationContract]
	    bool CheckPositionIsValid(Position position);
	    [OperationContract]
	    void SetVacantPosition(Position position);
	    [OperationContract]
	    void SendEmailVerification(string email);

    }
}
