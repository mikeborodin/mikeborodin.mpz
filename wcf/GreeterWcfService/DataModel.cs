﻿using System;
using System.Collections.ObjectModel;

namespace Donezilla.db
{
    public class DataModel
    {
        public ObservableCollection<Position> Tasks { get; set; }
    }

    public class Position
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }


    public class Employee
    {
        public long Id { get; set; }
        public long PositionId { get; set; }
        public DateTime Birthdate { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
    }
}