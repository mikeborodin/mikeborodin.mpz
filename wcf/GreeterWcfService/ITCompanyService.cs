﻿using Donezilla.db;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service

{
	public class ITCompanyService : IITCompanyService
	{
		bool IsSigned = false;
	
		DbHelper db = new DbHelper();

		public string Greet (string name)
		{
			return "Hello, " + name;
		}


		public bool SignIn (string email,string pass){
			if (email == "test@test.com" && pass == "1111") {
				IsSigned = true;
				return true;
			}
			return false 	;
		}

		public  bool SignOut (){

			if (IsSigned)
				return true;
			else
				return false;
		}

		public void CreatePosition(string title,string description){
			Position position = new Position () {
				Title = title,
				Description = description
			};
			Console.WriteLine ("\nAdding position "+position.Title);

			db.addPosition (position); 
		}

	    public async Task<List<Position>> GetPositions()
	    {
	        return db.GetPositionList();
	    }

	    public void UpdatePosition(Position position)
	    {
	        db.updatePosition(position);
	    }

	    public void DeletePosition(Position position)
	    {
	        db.deletePosition(position);
	    }

	    public bool CheckPositionIsValid(Position position)
	    {
	        throw new NotImplementedException();
	    }

	    public void SetVacantPosition(Position position)
	    {
	        throw new NotImplementedException();
	    }

	    public void SendEmailVerification(string email)
	    {
	        throw new NotImplementedException();
	    }
	}
}