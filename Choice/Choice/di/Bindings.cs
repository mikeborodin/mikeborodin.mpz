﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Choice.model;
using Choice.model.serializer;
using Ninject.Modules;

namespace Choice.di
{
    class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<ISerializer>()
                .To<MainJsonSerializer> ()                
                .InSingletonScope();
        }
    }
}
