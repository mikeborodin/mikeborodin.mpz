﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Choice.view.controls
{

    public class MyCustomControl : Control
    {
        static MyCustomControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MyCustomControl), new FrameworkPropertyMetadata(typeof(MyCustomControl)));
        }
        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register("Color",
            typeof(Color),
            typeof(MyCustomControl),
            new PropertyMetadata(Colors.Green));

        public Color Color
        {
            get
            {
                return (Color)this.GetValue(ColorProperty);
            }
            set
            {
                this.SetValue(ColorProperty, value);
            }
        }

        public static readonly ICommand CustomCommand = new RoutedUICommand("CustomCommand", "CustomCommand",
            typeof(MyCustomControl),
            new InputGestureCollection(
                new InputGesture[] {
                    new KeyGesture(Key.Enter),
                    new MouseGesture(MouseAction.LeftClick) }
            )
        );
     
    }
}
