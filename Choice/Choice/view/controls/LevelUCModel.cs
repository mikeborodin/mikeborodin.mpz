﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Choice.model;

namespace Choice.view.controls
{
    public class LevelUCModel
    {
        public Player  CurrentPlayer { get; set; }

        public int Level { get; set; }
    }
}
    