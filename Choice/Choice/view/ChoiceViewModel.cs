﻿using Choice.model;
using Choice.view.viewmodel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.view
{
    public class ChoiceListViewModel : viewmodel.ViewModelBase
    { 
        public ObservableCollection<ChoiceViewModel> Choices { get; set; }
        public ObservableCollection<PlayerViewModel> Players { get; set; }
    }
}
