﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.view.viewmodel
{
    public class ChoiceViewModel : ViewModelBase
    {
        private Guid _Id;
        public Guid Id { get { return _Id; } set { _Id = value; OnPropertyChanged("Id"); } }

        private String _ParentId;
        public String ParentId { get { return _ParentId; } set { _ParentId = value; OnPropertyChanged("ParentId"); } }

        private String _Description;
        public String Description { get { return _Description; } set { _Description = value; OnPropertyChanged("Description"); } }

        public String _Image;
        public String Image { get { return _Image; } set { _Image = value; OnPropertyChanged("Image");  } }
    }
}
