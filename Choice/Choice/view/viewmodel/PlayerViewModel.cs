﻿using Choice.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.view.viewmodel
{
    public class PlayerViewModel : ViewModelBase
    {

        private CharacterRace _Race;
        public CharacterRace Race { get {
                return _Race;
            } set {
                _Race = value;
                OnPropertyChanged("Race");
            }
        }
        //private Guid Id => Guid.NewGuid();

        private String _Username;
        public String Username { get {
                return _Username;
            } set {
                _Username = value;
                OnPropertyChanged("Username");
            }
        }

        private int _Level;
        public int Level { get {
                return _Level;
            } set {
                _Level = value;
                OnPropertyChanged("Level");
            } }
    }
}
