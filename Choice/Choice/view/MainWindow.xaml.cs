﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Choice.model;
using Choice.view;

namespace Choice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    { 
        public MainWindow()
        {
            InitializeComponent();
            Title = "Choice Game App";
            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            var player = new SoundPlayer(Properties.Resources.magic_loop);
            player.PlayLooping();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new SoundPlayer(Properties.Resources.confirm).Play();
            var secondWindow = new SecondWindow();
            secondWindow.Show();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            new SoundPlayer(Properties.Resources.confirm).Play();
            //new MainJsonSerializer.Save((ChoiceModel)DataContext);
        }
        private void OnExit_Click(object sender, RoutedEventArgs e)
        {
            SystemSounds.Beep.Play();
            System.Windows.Application.Current.Shutdown();
        }

      
    }
}
