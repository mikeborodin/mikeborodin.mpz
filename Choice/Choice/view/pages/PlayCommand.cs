﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;

namespace Choice.view.pages
{
    class PlayCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return parameter!=null;
        }

        public void Execute(object parameter)
        {
            //Console.WriteLine("Execute" + parameter.ToString());
            MessageBox.Show((String)parameter);
        }
    }
}
