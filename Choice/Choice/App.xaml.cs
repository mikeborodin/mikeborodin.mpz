﻿using Choice.model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using Choice.di;
using Choice.model.serializer;
using Choice.view;
using Ninject;
using Ninject.Modules;

namespace Choice
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        ChoiceModel Model { get; set; }
        ChoiceListViewModel ViewModel { get; set; }
        StandardKernel kernel = new StandardKernel();
        [Inject]
        public ISerializer serializer;

        public App() {

            kernel.Load(new List<NinjectModule>()
            {
                new Bindings()
            });

            //Model = kernel.Get<ISerializer>().Load();
            Model = MainJsonSerializer.GetInstance().Load();
            ViewModel = new ChoiceListViewModel();

            /*
             Model.Choices = new List<Choice.model.Choice>() {
                new model.Choice(){ Description = "Where to go, left or right? ", Id=Guid.NewGuid()},
                new model.Choice(){ Description = "Select Helmet or Shield? ", Id=Guid.NewGuid()},
                new model.Choice(){ Description = "Attack human or not? ", Id=Guid.NewGuid()}
            };
            */
            /*
             Model.Players = new List<Choice.model.Player>() {

                new model.Player(CharacterRace.Elf,"Eleron"){Level=1, Description = "A strong Elf!" },
                new model.Player(CharacterRace.Dwarf,"Ghimli"){Level=0,Description = "Son of Gloin"},
                new model.Player(CharacterRace.Human,"Aragorn"){Level=2,Description = "Son  of Arathorn"},

            };
           */
             

            //Console.WriteLine(Model.Choices[0].Description);

            initAutomapper();
            AutoMapper.Mapper.Map<ChoiceListViewModel>(Model);

            var view = new MainWindow() { DataContext = ViewModel};
            view.Show();

            
        }

        private void initAutomapper()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ChoiceModel, ChoiceListViewModel>();
               
            });
            AutoMapper.Mapper.Instance.Map(Model, ViewModel);

        }

        protected override void OnExit(ExitEventArgs e)
        {


            //AutoMapper.Mapper.Instance.Map(ViewModel,Model);
            kernel.Get<ISerializer>().Save(Model);
           
            base.OnExit(e);
        }
    }
}
