﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice
{
   public  class Benchmark
    {

        private int body = 0;
        static int Count = 1000000000;

        public void Public() {
            body++;
        }
        private void Private()
        {
            body++;
        }
        protected void Protected()
        {
            body++;
        }


        public long testPublic() {
            Stopwatch w = new Stopwatch();
            w.Start();
            for (var i = 0; i < Count; i++)
            {
                Public();
            }
            w.Stop();
            return w.ElapsedMilliseconds;
        }
        public long testPrivate()
        {
            Stopwatch w = new Stopwatch();
            w.Start();
            for (var i = 0; i < Count; i++)
            {
                Private();
            }
            w.Stop();
            return w.ElapsedMilliseconds;
        }
        public long testProtected()
        {
            Stopwatch w = new Stopwatch();
            w.Start();
            for (var i = 0; i < Count; i++)
            {
                Protected();
            }
            w.Stop();
            return w.ElapsedMilliseconds;
        }



        public InnerBench inner => new InnerBench();


        public class InnerBench {

            private int body = 0;
            public void Public()
            {
                body++;
            }

            private void Private()
            {
                body++;
            }
            public void Protected()
            {
                body++;
            }


            public long testPublic()
            {
                Stopwatch w = new Stopwatch();
                w.Start();
                for (var i = 0; i < Count; i++)
                {
                    Public();
                }
                w.Stop();
                return w.ElapsedMilliseconds;
            }
            public long testPrivate()
            {
                Stopwatch w = new Stopwatch();
                w.Start();
                for (var i = 0; i < Count; i++)
                {
                    Private();
                }
                w.Stop();
                return w.ElapsedMilliseconds;
            }
            public long testProtected()
            {
                Stopwatch w = new Stopwatch();
                w.Start();
                for (var i = 0; i < Count; i++)
                {
                    Protected();
                }
                w.Stop();
                return w.ElapsedMilliseconds;
            }
        }
    }
}
