﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Choice.model
{
    [DataContract]   
    public class Choice
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public String ParentId { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String Image { get; set; }

        [DataMember]
        public String choiceA { get; set; }

        [DataMember]
        public String choiceB { get; set; }


        public void Calculate(Choice c,out String result,ref int r) {
            result = "Hello";
            r++;
        }
        


    }
}
