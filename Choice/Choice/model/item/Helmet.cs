﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.item
{
    public class Helmet : AbsItem, IWearable
    {
        public Helmet()
        {
        }

        public override int GetCost()
        {
            return 50; ;
        }

        public override int GetWeight()
        {
            return 5; ;
        }

        public void wear()
        {
            throw new NotImplementedException();
        }
    }
}
