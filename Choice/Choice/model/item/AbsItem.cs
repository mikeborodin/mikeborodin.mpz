﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model
{
    public abstract class AbsItem
    {
        public abstract int GetWeight();
        public abstract int GetCost();


        public override string ToString()
        {
                return "Anstract class AbsItem";
        }
    }
}
