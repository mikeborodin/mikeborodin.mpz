﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.item
{
    public class Drink : AbsItem,IEatable
    {
        
        protected Drink(int alcohol)
        {
            Alcohol = alcohol;
        }

        public int Alcohol { get; set; }
        public int Volume { get; set; }

        public void eat()
        {
            throw new NotImplementedException();
        }

        public override int GetCost()
        {
            return 1;
        }

        public override int GetWeight()
        {
            return 3;
        }
    }
}
