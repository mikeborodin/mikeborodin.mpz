﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.item
{
    public class Food : AbsItem, IEatable
    {
        public Food(int energy) {
            Energy = energy;
        }

        int Energy { get; set; }
        int Taste { get; set; }

        public void eat()
        {
            throw new NotImplementedException();
        }

        public override int GetCost()
        {
            return 10;
        }

        public override int GetWeight()
        {
            return 1;
        }
    }
}
