﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reactive;
using System.Runtime.InteropServices.ComTypes;

namespace Choice.model.dp.mediator
{
    public class PlayerMediator
    {
        public IDictionary<string,Player> Players = new Dictionary<string,Player>();

        public string AddPlayer(Player p)
        {
            var id = Guid.NewGuid();
            Players.Add(id.ToString(),p);

            var observer = Observer.Create<Point>(point =>
               Console.WriteLine(point.ToString())
            ); 
            Players[id.ToString()].Position.Subscribe();

            return id.ToString();
        }
        public void updatePlayersPosition(string id,Point point)
        {
            Players[id].Position.OnNext(point);
        }
    }
}