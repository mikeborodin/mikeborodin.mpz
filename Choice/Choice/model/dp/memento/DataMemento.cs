﻿namespace Choice.model.dp.memento
{
    public class DataMemento
    {
        public ChoiceModel Data { get; set; }

        public void SaveData()
        {
            var serializer = new MainJsonSerializer();
            serializer.Save(Data);
        }

        public void LoadData()
        {
            var serializer = new MainJsonSerializer();
            Data = serializer.Load();
        }
    }
}