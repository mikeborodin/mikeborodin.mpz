﻿namespace Choice.model.dp.memento
{
    public class DataOrginator
    {
        public ChoiceModel Data { get; set; }

        public DataMemento getDataMemento()
        {
            var memento = new DataMemento();
            memento.Data = Data;
            return memento;
        }
    }
}