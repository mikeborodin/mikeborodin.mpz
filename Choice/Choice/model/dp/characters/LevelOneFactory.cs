﻿using Choice.model.dp.entity;

namespace Choice.model.dp.randomchar
{
    public class LevelOneFactory : AbsCharacterFactory
    {
        public override AbsMortalCharacter CreateMortalCharacter()
        {
            return new Humanoid("eregorn");
        }

        public override AbsImmortalCharacter CreateImmortalCharacter()
        {
            return new DarkGhost("nazghul");
        }
    }
}