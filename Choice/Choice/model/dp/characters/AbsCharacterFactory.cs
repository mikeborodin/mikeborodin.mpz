﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Choice.model.dp.entity;

namespace Choice.model.dp.randomchar
{
    public abstract class AbsCharacterFactory
    {
        public abstract AbsMortalCharacter CreateMortalCharacter();
        public abstract AbsImmortalCharacter CreateImmortalCharacter();
    }
}
