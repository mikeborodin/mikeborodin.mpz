﻿using Choice.model.dp.entity;

namespace Choice.model.dp.randomchar
{
    public class LevelTwoFactory : AbsCharacterFactory
    {
        public override AbsMortalCharacter CreateMortalCharacter()
        {
            return new Bird("eagle");
        }

        public override AbsImmortalCharacter CreateImmortalCharacter()
        {
            return new Elf("elendor");
        }
    }
}