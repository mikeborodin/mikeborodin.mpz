﻿using System;
using Choice.model.dp.entity;

namespace Choice.model.dp.randomchar
{
    public class RandomCharacterGenerator
    {
        public static Character GenerateImmortalCharacters(int level)
        {

            AbsCharacterFactory factory = null;
            switch (level)
            {
                case 1:
                    factory = new LevelOneFactory();
                    break;
                case 2:
                    factory = new LevelTwoFactory();
                    break;
                default: throw new Exception("illigal argumnet");
            }
            return factory.CreateImmortalCharacter();
        }



        public static Character GenerateMortalCharacters(int level)
        {

            AbsCharacterFactory factory = null;
            switch (level)
            {
                case 1:
                    factory = new LevelOneFactory();
                    break;
                case 2:
                    factory = new LevelTwoFactory();
                    break;
                default: throw new Exception("illigal argumnet");
            }
            return factory.CreateMortalCharacter
                
                
                ();
        }
    }
}