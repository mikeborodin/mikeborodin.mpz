﻿using Choice.model.dp.facade;

namespace Choice.model.dp
{
    public class ChoiceGameFacade
    {
        public void authenticate()
        {
            var auth = new FireAuthClient();
            auth.signInWithEmailAndPassword("test","test");
        }
        public void udateRecord(Player p)
        {
            var an = new AnalyticsClient();
            an.logEvent("Record_Updated");
            
            var db = new DBHelper();
            db.addRecord(p.Level);
        }
    }
}