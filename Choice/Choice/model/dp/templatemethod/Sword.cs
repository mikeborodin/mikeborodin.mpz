﻿namespace Choice.model.dp.templatemethod
{
    public class Sword:Weapon
    {
        override public void Attack(Player player)
        {
            player.Level -= 2;
        }
    }
}