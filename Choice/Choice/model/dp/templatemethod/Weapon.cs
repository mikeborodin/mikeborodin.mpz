﻿namespace Choice.model.dp.templatemethod
{
    public class Weapon
    {
        public int Damage { get; set; } = 0;
        public int Weight { get; set; } = 1;
        public int Cost { get; set; } = 2;

        public void Use(Player player)
        {
            Attack(player);
            UpdateDamage();
        }

        private void UpdateDamage()
        {
            Damage++;
        }

        public virtual void Attack(Player player)
        {
            player.Level--;
        }
    }
}