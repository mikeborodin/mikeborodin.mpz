﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Choice.model.characters;

namespace Choice.model.dp.entity
{
    public class AbsMortalCharacter : Character, IAttackable
    {

        public int Health { get; set; }
        public int Age { get; set; }


        public AbsMortalCharacter(string name) : base(name)
        {
            Age = 1;
            Health = 100;
        }

        public override void Greet()
        {
            Console.WriteLine(Name);
        }


        public int attack()
        {
            throw new NotImplementedException();
        }
    }
}
