﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.dp.entity
{
    class DarkGhost :AbsImmortalCharacter
    {
        public DarkGhost(string name) : base(name)
        {
            GhostPower = 600;
        }


        public void MakeDarkMagic()
        {
            GhostPower *= 2;
            Console.WriteLine("MakeDarkMagic");
        }

        public void SayBooo()
        {
            Console.WriteLine("BOOOOO!");
        }
    }
}
