﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Choice.model.item;

namespace Choice.model.dp.entity
{
    class Humanoid :AbsMortalCharacter
    {
        public Humanoid(string name) : base(name)
        {
        }

        public AbsItem GetItem()
        {
            /* Returns a new item*/
            return new Food(100);
        }
    }
}
