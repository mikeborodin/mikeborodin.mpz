﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.dp.entity
{
    public abstract class Character
    {
        public Character(String name) {
            Name = name;
        }

        protected String Name { get; set; }
        protected List<AbsItem>  Items{ get; set; } 

  
        public abstract void Greet();

        public override string ToString()
        {
            return base.ToString()+" AbsChar";
        }
  
    }
}
