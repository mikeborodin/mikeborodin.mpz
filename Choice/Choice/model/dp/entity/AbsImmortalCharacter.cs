﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.dp.entity
{
    public class AbsImmortalCharacter : Character
    {
        public int GhostPower { get; set; }

        public AbsImmortalCharacter(string name) : base(name)
        {
            GhostPower = 500;
        }

        public override void Greet()
        {
            Console.WriteLine("Ghost "+Name);
        }
    }
}
