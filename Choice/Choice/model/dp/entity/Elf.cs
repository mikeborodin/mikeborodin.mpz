﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Choice.model.item;

namespace Choice.model.dp.entity
{
    class Elf: AbsImmortalCharacter
    {
        public Elf(string name) : base(name)
        {
        }

        public void MakeLightFireball()
        {
            GhostPower *= 3;
            Console.WriteLine("MakeLightFireball");
        }

        public void SayHello()
        {
            Console.WriteLine("Heelo Im" + Name);
        }

        public AbsItem GiveGifts()
        {
            return new Helmet();
        }

    }
}
