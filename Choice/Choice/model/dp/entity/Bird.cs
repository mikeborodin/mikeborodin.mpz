﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.dp.entity
{
    class Bird :AbsMortalCharacter
    {

        public Bird(string name) : base(name)
        {
            Console.WriteLine("Created Bird");
        }

        public void FlyTo()
        {
            Console.WriteLine("Flying");
        }
    }
}
