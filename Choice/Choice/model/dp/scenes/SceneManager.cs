﻿using System;

namespace Choice.model.dp.scenes
{
    public class SceneManager
    {
        private IPrototype ItemPrototype {
            get;
            set;
        }

        public AbsItem MyItem;


        public SceneManager()
        {

        }

        public void AddItemsToScene()
        {

            ItemPrototype = new FoodPrototype(10);
            MyItem = ItemPrototype.Clone();

            ItemPrototype = new ArmorPrototype();
            MyItem = ItemPrototype.Clone();
        }
    }
}