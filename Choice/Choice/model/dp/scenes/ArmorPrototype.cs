﻿using Choice.model.item;

namespace Choice.model.dp.scenes
{
    public class ArmorPrototype: Helmet, IPrototype   {

        

        public override int GetWeight()
        {
            return 5;
        }

        public override int GetCost()
        {
            return 100;
        }

        public AbsItem Clone()
        {
           return  (AbsItem)this.MemberwiseClone();
        }
    }
}