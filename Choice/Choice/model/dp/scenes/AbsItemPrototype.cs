﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Choice.model;

namespace Choice.model.dp.scenes
{
    public interface IPrototype
    {
        AbsItem Clone();
    }
}
