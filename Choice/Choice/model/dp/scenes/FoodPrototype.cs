﻿using Choice.model.item;

namespace Choice.model.dp.scenes
{
    public class FoodPrototype: Food, IPrototype   {

        public FoodPrototype(int energy) : base(energy)
        {

        }

        public override int GetWeight()
        {
            return 1;
        }

        public override int GetCost()
        {
            return 10;
        }

        public AbsItem Clone()
        {
           return  (AbsItem)this.MemberwiseClone();
        }
    }
}