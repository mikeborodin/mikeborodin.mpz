﻿using System;

namespace Choice.model.dp.facade
{
    public class AnalyticsClient
    {
        public void logEvent(string name)
        {
            Console.WriteLine(name);
        }
    }
}