﻿using System.Drawing;

namespace Choice.model.dp.fly
{
    public class ItemFlyweight
    {
        public Point Position { get; set; }
        public Icon Image{ get; set; }

        public bool isVisible { get; set; } = true;
        public bool isAvailable { get; set; } = true;
        public int Cost { get; set; } = 100;
        public int Weight { get; set; } = 10;
    }
}