﻿namespace Choice.model.dp.fly
{
    public class ItemManager
    {
        public ItemManager()
        {
            Factory = new ItemFlyweightFactory();
        }


        private ItemFlyweightFactory Factory;
        public void spawnItems()
        {
            string path0 = "D:/icons/food_0.jpg";
            string path1 = "D:/icons/food_1.jpg";
            string path2 = "D:/icons/food_2.jpg";

            for (var i = 0; i < 100; i++)
            {
                if (i < 33)
                {
                    var item = Factory.getFlyweight(path0);
                }
                if (i < 66 && i> 33)
                {
                    var item = Factory.getFlyweight(path1);
                }
                if (i > 66)
                {
                    var item = Factory.getFlyweight(path2);
                }
            }
        }
        
    }
}