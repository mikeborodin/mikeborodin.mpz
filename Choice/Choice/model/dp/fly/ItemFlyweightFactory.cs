﻿using Ninject.Activation.Caching;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Data;

namespace Choice.model.dp.fly
{
    public class ItemFlyweightFactory
    {
        public Dictionary<string, ItemFlyweight> Cache { get; set; }

        public ItemFlyweight getFlyweight(string image)
        {
            if (Cache.ContainsKey(image))
            {
                return Cache[image];
            }
            Cache[image] = new ItemFlyweight();
            Cache[image].Image = new Icon(image);

            return Cache[image];
        }



    }

}