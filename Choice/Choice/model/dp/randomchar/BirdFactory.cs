﻿using Choice.model.dp.entity;

namespace Choice.model.dp.randomchar
{
    public class BirdFactory : AbsCharacterFactory
    {
        public override Character CreateCharacter()
        {
            return new Bird("eagle");
        }
    }
}