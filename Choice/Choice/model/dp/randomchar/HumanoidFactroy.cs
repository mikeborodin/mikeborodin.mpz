﻿using Choice.model.dp.entity;

namespace Choice.model.dp.randomchar
{
    public class HumanoidFactroy :AbsCharacterFactory
    {
        public override Character CreateCharacter()
        {
            return new Humanoid("boromir");
        }
    }
}