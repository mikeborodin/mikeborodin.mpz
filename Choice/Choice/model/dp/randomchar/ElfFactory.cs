﻿using Choice.model.dp.entity;

namespace Choice.model.dp.randomchar
{
    public class ElfFactory : AbsCharacterFactory
    {
        public override Character CreateCharacter()
        {
            return new Elf("elendor");
        }
    }
}