﻿using System;
using Choice.model.dp.entity;

namespace Choice.model.dp.randomchar
{
    public class RandomCharacterGenerator
    {
        public static Character GenerateRanomCharacter()
        {
            Random r = new Random(4);
            AbsCharacterFactory factory = null;

            int ind = (int) r.NextDouble();
            switch (ind)
            {
                case 1:
                    factory = new HumanoidFactroy();
                    break;
                case 2:
                    factory = new BirdFactory();
                    break;
                case 3:
                    factory = new DarkGhostFactory();
                    break;
                default:
                    factory = new ElfFactory();
                    break;

            }
            return factory.CreateCharacter();
        }
    }
}