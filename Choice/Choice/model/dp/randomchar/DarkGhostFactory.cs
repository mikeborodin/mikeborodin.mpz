﻿using Choice.model.dp.entity;

namespace Choice.model.dp.randomchar
{
    public class DarkGhostFactory :AbsCharacterFactory
    {
        public override Character CreateCharacter()
        {
            return new DarkGhost("nazghul");
        }
    }
}