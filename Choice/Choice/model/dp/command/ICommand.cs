﻿namespace Choice.model.dp.command
{
    public interface ICommand
    {
        void execute();
    }
}