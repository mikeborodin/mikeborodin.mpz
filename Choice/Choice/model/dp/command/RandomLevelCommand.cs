﻿using System.Data.Entity.Infrastructure;
using Choice.model.dp.randomchar;

namespace Choice.model.dp.command
{
    public class RandomLevelCommand:ICommand
    {
        public string Description => "Test choice!!!";
        public RandomLevelReciever Reciever { get; set; }

        public void execute()
        {
            var choice = new Choice();
            choice.Description = Description;
            Reciever.Action(choice);
        }
    }
}