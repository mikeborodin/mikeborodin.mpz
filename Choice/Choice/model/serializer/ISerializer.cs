﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.serializer
{
    public interface ISerializer
    {
        ChoiceModel Load();
        void Save(ChoiceModel model);
    }
}
