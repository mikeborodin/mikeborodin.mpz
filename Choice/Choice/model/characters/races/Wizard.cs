﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.characters
{
    public class Wizard : AbsCharacter
    {


        public Wizard(string name) : base(name)
        {
        }

        public int Kindness { get; set; }
        //public SuperPower Power{ get; set; }

         
        public override void Greet()
        {
            Console.WriteLine("Hello! I'm a wizard ", this.Name);
        }
    }

    class SuperPower {
        public void FireBall() {
            Console.WriteLine("Fireball!!!");
        }
    }
}
