﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.characters
{

    public struct HumanStruct{
        string name;
        int Health;

        public int attack()
        {
            return Health--;
        }

        public override string ToString()
        {
            return base.ToString() + " Human";
        }

        public  void Greet()
        {
            Console.WriteLine("Hi there, my name is " );
        }
         
    }
    public class Human : AbsCharacter, IAttackable
    {
        public Human(string name) : base(name)
        {

            Console.WriteLine("Hi there, created new man "+Name);
        }

        public int attack()
        {
            return Health--;
        }

        public override string ToString()
        {
            return base.ToString()+" Human";
        }

        public override void Greet()
        {
            Console.WriteLine("Hi there, my name is " + Name);
        }

        public static explicit operator Human(Wizard ob)
        {
            return new Human("mike")
            {
                Health = 100,
                Age = 21,
            };
        }

        public static implicit operator Wizard(Human ob)
        {
            return new Wizard("Gandalf");
        }

    }
}