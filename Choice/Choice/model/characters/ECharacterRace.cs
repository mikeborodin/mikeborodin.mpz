﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model
{
    [Flags]
    public enum CharacterRace
    {
        Human,
        Elf,
        Wizard,
        Dwarf,
        Orc
    }
}
