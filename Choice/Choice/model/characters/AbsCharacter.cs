﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.characters
{
    public abstract class AbsCharacter:IComparable
    {
        public AbsCharacter(String name) {
            Name = name;
        }
        CharacterRace Race { get; set; }

        protected String Name { get; set; }
        protected int  Health { get; set; }
        protected int Age { get; set; }

    
        public abstract void Greet();

        public int compare(object o)
        {
            return Object.Equals(this, o) ? 0 : -1;
        }

        public override string ToString()
        {
            return base.ToString()+" AbsChar";
        }
        public override bool Equals(object obj)
        {
            return !base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
