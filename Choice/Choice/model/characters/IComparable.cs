﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model.characters
{
    public interface IComparable
    {
        int compare(Object o);
    }
}
