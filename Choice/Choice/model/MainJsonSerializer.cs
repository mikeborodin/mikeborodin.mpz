﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Choice.model.serializer;

namespace Choice.model
{
    public class MainJsonSerializer : ISerializer
    {

        private static MainJsonSerializer Instance;

        public static MainJsonSerializer GetInstance()
        {
            if (Instance == null) return new MainJsonSerializer();
            else return Instance;
        }
        
        public ChoiceModel Load() {


            string path = @"C:\Users\Roman\choice.json";
           //string path = @"C:\Users\admin\choice.json";
            //string path = @"D:\choice.json";

            try
            {

                var file = new FileStream(path, FileMode.Open);
                return (ChoiceModel)new DataContractJsonSerializer(typeof(ChoiceModel))
                .ReadObject(file);
            } catch (Exception e) {
                return new ChoiceModel();
            }   
        }
        public void Save(ChoiceModel model)
        {
            string path = @"C:\Users\admin\choice.json";

            //string path = @"C:\users\Roman\choice.json";
            //string path = @"D:\choice.json";
            new DataContractJsonSerializer(typeof(ChoiceModel))
                .WriteObject(
                    new FileStream(path, FileMode.Create),model);
        }


    }
}
