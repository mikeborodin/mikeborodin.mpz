﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Choice;
using System.Runtime.Serialization;

namespace Choice.model
{
    [DataContract]
   public  class ChoiceModel
    {
    [DataMember]
       public IList<Choice> Choices { get; set; }
        [DataMember]
        public IList<Player> Players { get; set; }
    }
}
