﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reactive.Subjects;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Choice.model
{
    [DataContract]
    public  class Player
    {
        static Player() {
            Console.WriteLine("static constructor");
            //Id = Guid.NewGuid();
        }

        public ISubject<Point> Position { get; set; }

        [DataMember]
        public  CharacterRace Race { get; set; }
        [DataMember]
        private Guid Id { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public int Level { get; set; }
        [DataMember]
        public string Description { get; set; }

        public Player(CharacterRace race, string username) {
            Race = race;
            Username = username;
        }

        public override string ToString()
        {
            return "Player with id "+Id;
        }
    }
}
