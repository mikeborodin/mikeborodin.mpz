﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Collections.Generic;
using Choice.entities.characters;

namespace Choice.entities
{
    [TestClass]
    public class TestMain
    {
        [TestMethod]
        public void TestMethod1()
        {
            /*
              Player player = new Player(CharacterRace.Elf, "mike");
             player.Username = "Elf";

             Object boxedRaceELf = (object)CharacterRace.Elf;
             int elfIndexInEnum = (int) (CharacterRace) boxedRaceELf;

             CharacterRace race = CharacterRace.Elf | CharacterRace.Human;
             bool isElf = (race & CharacterRace.Dwarf) != 0;
             CharacterRace typeTwo = race & ~CharacterRace.Dwarf;
             CharacterRace typeThree = race ^ CharacterRace.Dwarf;

              */

            const int count = 1000000;
            Stopwatch watch = new Stopwatch();
            watch.Start();


            /*
             var bench = new Benchmark();

            long pub = bench.testPublic();
            long pri = bench.testPrivate();
            long pro = bench.testProtected();


            long pubInner = bench.inner.testPublic();
            long priInner = bench.inner.testPrivate();
            long proInner = bench.inner.testProtected();


            Console.WriteLine(pub);
            Console.WriteLine(pri);
            Console.WriteLine(pro);

            Console.WriteLine(pubInner);
            Console.WriteLine(priInner);
            Console.WriteLine(proInner);
             */


            Stopwatch w = new Stopwatch();
            w.Start();
            var list = new List<Choice>(10000000);
            w.Stop();
            var choiceTime = w.ElapsedMilliseconds;

            w.Start();
            var list2 = new List<Human>(10000000);
            w.Stop();
            var humanTime = w.ElapsedMilliseconds;

            w.Start();
            var list3 = new List<HumanStruct>(10000000);
            w.Stop();
            var humanStructTime = w.ElapsedMilliseconds;


            Console.WriteLine(choiceTime);
            Console.WriteLine(humanTime);
            Console.WriteLine(humanStructTime);


            Assert.IsTrue(true);
        }
    }
}
